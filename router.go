package main

import (
	"github.com/gin-gonic/gin"
)

func ConfigureRoutes(r gin.Engine) {
	r.GET("/", GetLatestTemps)
	r.GET("/graph", GetGraphForTime)
	r.Static("/static", "./public")
}

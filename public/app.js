// Generic constants
const degreesF = '°F';

// DOM constants
const cursorElementId = 'cursor';
const currentTimeId = 'current-time';
const burgerId = 'burger';
const mainNavId = 'mainNavbar'
const graphElementId = 'graph';
const dataLookupElementId = 'data-lookup';
const hoveredDateId = 'current-time';
const xAxisLegend = 'x-axis-legend';
const xAxisTicks = 'x-axis-ticks';
const yAxisTicks = 'y-axis-ticks';
const xAxisPad = 40;
const yAxisPad = 40;

// Backend data
let sharedData = {};
let svgContext = null;

// Keep track of the current width/height of the svg
class SvgContext {
  constructor(width, height, minTemp, maxTemp, minTime, maxTime) {
    this.width = width;
    this.height = height;
    this.minTemp = minTemp;
    this.maxTemp = maxTemp;
    this.minTime = minTime;
    this.maxTime = maxTime;
  }

  // Scale a temperature to an amount of px
  tempToPx(temp) {
    const tempRange = this.maxTemp - this.minTemp;
    return Math.floor(this.height - (((temp - this.minTemp) / tempRange) * this.height));
  }

  // Scale a time to an amount of px
  timeToPx(time) {
    const timeWidth = this.maxTime - this.minTime;
    const timeOffset = time - this.minTime;
    const percent = timeOffset / timeWidth;
    const px = percent * this.width;
    return (xAxisPad + px).toFixed(2);
  }

  // Convert px hover value on svg to a time
  pxToTime(px) {
    const timeWidth = this.maxTime - this.minTime;
    const percent = (px - xAxisPad) / this.width;
    const time = (timeWidth * percent) + this.minTime;
    return time;
  }
}

const renderSvgLine = (c, sensorId, data) => {
  let op = 'M'
  let firstInstructionDone = false;
  data.sort((a, b) => b.Time - a.Time);
  let instruction = ''
  for (let v of data) {
    const xpx = c.timeToPx(v.Time);
    const ypx = c.tempToPx(v.Temperature);
    instruction = `${instruction}${op}${xpx},${ypx} `;
    if (!firstInstructionDone) {
      firstInstructionDone = true;
      op = 'L'
    }
  }
  return instruction;
};

const anHourAndOneMinute = (60 * 61) * 10;
const twentyFourHours = (24 * 60 * 60) * 10;
const aWeek = 7 * twentyFourHours;

const redrawCursor = c => {
  const existingCursor = document.getElementById("cursor");
  const dataFragments = existingCursor.getAttribute('d').split(' ');
  const heightRemoved = dataFragments.slice(0, dataFragments.length - 1);
  existingCursor.setAttribute('d', [...heightRemoved, `${c.height}`].join(' '));
}

const redrawTemperatureLegend = (c) => {
  const existingLegendElements = document.getElementById(xAxisLegend);
  if (existingLegendElements != null && existingLegendElements.length != 0) {
    [...existingLegendElements.children].forEach(el => {
      if (el.hasAttribute('data-temperature')) {
        const temp = parseInt(el.getAttribute('data-temperature'));
        if (temp < c.minTemp || temp > c.maxTemp) {
          existingLegendElements.removeChild(el)
        } else {
          el.setAttribute('y', c.tempToPx(temp));
        }
      }
    });
  }
};

const reDrawAxisLines = (c) => {
  // Destroy current lines
  let xTicksContainer = document.getElementById(xAxisTicks);
  let yTicksContainer = document.getElementById(yAxisTicks);
  [...xTicksContainer.children].forEach(c => c.remove());
  [...yTicksContainer.children].forEach(c => c.remove());

  const start = Math.floor(c.minTemp);
  const end = Math.ceil(c.maxTemp);
  for (let i = start; i < end; i++) {
    // Make a horizontal line
    const px = c.tempToPx(i);
    const line = document.createElementNS('http://www.w3.org/2000/svg', 'path');
    line.setAttribute('d', `M ${yAxisPad} ${px} H ${c.width}`);
    line.setAttribute('fill', 'none');
    if (i === 68) {
      line.setAttribute('stroke', 'maroon');
      line.setAttribute('strokewidth', '2');
    } else {
      line.setAttribute('stroke', 'lightgray');
      line.setAttribute('strokewidth', '1');
    }

    xTicksContainer.appendChild(line);
  }


  const timeDiff = c.maxTime - c.minTime
  let timeStepSize = 0;
  if (timeDiff > 0 && timeDiff < anHourAndOneMinute) {
    timeStepSize = 5 * 60 // 5 minutes
  } else if (timeDiff >= anHourAndOneMinute && timeDiff < twentyFourHours) {
    timeStepSize = 60 * 60 // An hour
  } else if (timeDiff >= twentyFourHours && timeDiff < aWeek) {
    timeStepSize = 4 * 60 * 60 // Four hours
  } else {
    timeStepSize = 24 * 60 * 60
  }

  for (let i = c.minTime; i < c.maxTime; i = i + timeStepSize) {
    let px = c.timeToPx(i);
    const line = document.createElementNS('http://www.w3.org/2000/svg', 'path');
    line.setAttribute('d', `M ${px} 0 V ${c.height}`);
    line.setAttribute('fill', 'none');
    line.setAttribute('stroke', 'lightgray');
    line.setAttribute('strokewidth', '1');
    yTicksContainer.appendChild(line);
  }
};

const redrawGraph = () => {
  reDrawAxisLines(svgContext);
  redrawTemperatureLegend(svgContext);
  redrawCursor(svgContext);
  const sensors = Object.keys(sharedData.Data);
  for (let sensor of sensors) {
    let el = document.getElementById(`line-${sensor}`);
    el.setAttribute('d', renderSvgLine(svgContext, sensor, sharedData.Data[sensor].Data));
  }
};

const prettyDate = (fdate) => {
  let date = fdate;
  if (date < 2000000000) {
    date = date * 1000;
  }
  const d = typeof date === 'object' ? date : new Date(date);
  const month = d.getMonth() + 1;
  const day = d.getDate();
  const hour = d.getHours();
  const minutes = d.getMinutes();
  const amPm = hour >= 12 ? 'PM' : 'AM';
  const displayHours = hour == 12 || hour == 0 ? 12 : hour % 12;
  const displayMinutes = minutes < 10 ? `0${minutes}` : `${minutes} ${amPm}`

  return `${month}/${day} ${displayHours}:${displayMinutes}`;
};

const handleCursorEnter = (_e) => {
  const cursorEl = document.getElementById(cursorElementId);
  if (cursorEl.hasAttribute('style')) {
    cursorEl.removeAttribute('style');
  }
};

const handleCursorLeave = (_e) => {
  const cursorEl = document.getElementById(cursorElementId);
  if (!cursorEl.hasAttribute('style')) {
    cursorEl.setAttribute('style', 'display: none;');
  }
};

const handleSvgTouchStart = (_e) => {
  const cursorEl = document.getElementById(cursorElementId);
  if (cursorEl.hasAttribute('style')) {
    cursorEl.removeAttribute('style');
  }
};

const handleSvgTouchEnd = (touchEv) => {
  const touch = touchEv.changedTouches[0];
  const x = touch.clientX;
  if (x > xAxisPad) {
    const cursorEl = document.getElementById(cursorElementId);
    cursorEl.setAttribute('d', `M ${x} 0 V ${svgContext.height || 800}`);
  }

  renderTempsForCursor(svgContext, x);
};

const handleTouchOnSvg = (touchEv) => {
  const touch = touchEv.touches[0];
  const x = touch.clientX;
  if (x > xAxisPad) {
    const cursorEl = document.getElementById(cursorElementId);
    cursorEl.setAttribute('d', `M ${x} 0 V ${svgContext.height || 800}`);
  }

  renderTempsForCursor(svgContext, x);
}

const handleCursorMoveOnSvg = (ev) => {
  const rect = ev.target.getBoundingClientRect();
  const x = ev.clientX - rect.left;
  if (x > xAxisPad) {
    const cursorEl = document.getElementById(cursorElementId);
    cursorEl.setAttribute('d', `M ${x} 0 V ${svgContext.height || 800}`);
  }

  renderTempsForCursor(svgContext, x);
};

const calculateTempsForX = (c, x) => {
  const time = c.pxToTime(x);
  const sensorKeys = Object.keys(sharedData.Data);
  const temps = sensorKeys.map(k => {
    const dataForSensor = sharedData.Data[k].Data;
    const closestTemp = dataForSensor.reduce((acc, i) => {
      const d = i.Time;
      if (acc.Time === -1 || Math.abs(d - time) < Math.abs(acc.Time - time)) {
        return i;
      }
      return acc;
    }, { Temperature: -1, Time: -1 });

    return { id: k, temp: Math.round(closestTemp.Temperature * 100) / 100, time: prettyDate(closestTemp.Time) };
  });

  return temps;
};

const renderTempsForCursor = (c, x) => {
  const temps = calculateTempsForX(c, x);
  for (const temp of temps) {
    const legendElement = document.getElementById(`temp-${temp.id}`);
    legendElement.innerText = `${temp.temp}${degreesF}`;
  }

  const dateOutputElement = document.getElementById(currentTimeId);
  dateOutputElement.textContent = `${temps[0].time}`;
};

const main = () => {
  const data = JSON.parse(window.rawSharedData);
  const meta = JSON.parse(window.rawMeta);
  sharedData = data;
  sharedData.meta = meta;
  const allSensorKeys = Object.keys(sharedData.Data);
  for (const sensorKey of allSensorKeys) {
    sharedData.Data[sensorKey].Data = sharedData.Data[sensorKey].Data.sort((a, b) => a.Time - b.Time).map(x => ({ ...x, time: x.Time * 1000, temperature: x.Temperature }));
  }
  const cursorReader = document.getElementById(graphElementId);
  svgContext = new SvgContext(
    Math.floor(cursorReader.clientWidth),
    Math.floor(cursorReader.clientHeight),
    sharedData.meta.MinTemp,
    sharedData.meta.MaxTemp,
    sharedData.meta.MinTime,
    sharedData.meta.MaxTime
  );

  redrawGraph();

  cursorReader.addEventListener('mouseenter', handleCursorEnter);
  cursorReader.addEventListener('mouseleave', handleCursorLeave);
  cursorReader.addEventListener('mousemove', handleCursorMoveOnSvg);
  //cursorReader.addEventListener('touchmove', handleCursorMoveOnSvg);
};

document.addEventListener('DOMContentLoaded', (_e) => {
  const burgerElement = document.getElementById(burgerId);
  burgerElement.addEventListener('click', (_ev) => {
    const target = document.getElementById(mainNavbar);
    target.classList.toggle('is-active');
    burgerElement.classList.toggle('is-active');
  });

  let timeout = null;
  window.addEventListener('resize', (_e) => {
    if (timeout != null) {
      clearTimeout(timeout);
    }
    timeout = setTimeout(() => {
      const cursorReader = document.getElementById('graph');
      svgContext.width = Math.floor(cursorReader.clientWidth);
      svgContext.height = Math.floor(cursorReader.clientHeight);
      redrawGraph();
    }, 250);
  });

  const graph = document.getElementById('graph');
  graph.addEventListener('touchend', handleSvgTouchEnd);
  graph.addEventListener('touchmove', handleTouchOnSvg);

  main();
});

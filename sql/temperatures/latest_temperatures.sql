select
    s.id, s.description as name,
    (
        select r.temperature
        from readings r
        where r.sensor_id = s.id
        order by r.reading_date desc
        limit 1
    ) as temperature, 
    (
        select
        extract(epoch from date_trunc('minute', r.reading_date))
        from readings r
        where r.sensor_id = s.id
        order by r.reading_date desc
        limit 1
    ) as reading_date
from sensors s
order by s.id
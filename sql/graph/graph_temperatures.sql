select
    s.id,
    s.description as name,
    i.reading_date,
    avg(i.temperature) as temperature
from (
    select
    r.temperature as temperature,
    r.sensor_id,
    (floor(extract(epoch from r.reading_date) / extract(epoch from interval '$1' )) *
    extract(epoch from interval '$1' )) as reading_date
    from readings r
    where r.reading_date > (current_timestamp at time zone 'utc' + interval '$2' )
) i
join sensors s on s.id = i.sensor_id
group by s.id, s.description, i.reading_date
order by s.id, i.reading_date

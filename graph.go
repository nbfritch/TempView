package main

import (
	"encoding/json"
	"fmt"
	"html/template"
	"math"
	"net/http"
	"sort"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
)

const (
	oneHour  = "hour"
	oneDay   = "day"
	oneWeek  = "week"
	oneMonth = "month"
)

var sensorColors = []string{
	"green",
	"blue",
	"orange",
	"purple",
	"red",
}

func getSensorColorForId(id int64) string {
	return sensorColors[(id%int64(len(sensorColors)))-1]
}

// Information for graph legend
type SensorLegend struct {
	Id          int64
	Name        string
	Color       string
	InitialTemp float64
}

// Rows as they come from db
type GraphQueryRow struct {
	Id          int64
	Name        string
	ReadingDate float64
	Temperature float64
}

// Single `point`
type GraphPoint struct {
	Time        int64
	Temperature float64
}

// Many points + attached name
type SensorData struct {
	Name string
	Data []GraphPoint
}

// Map of sensorId => Data
type GraphDataSet struct {
	Data map[int64]SensorData
}

func (g *GraphDataSet) addRow(row GraphQueryRow) {
	point := GraphPoint{Time: int64(row.ReadingDate), Temperature: row.Temperature}
	if sensorData, ok := g.Data[row.Id]; ok {
		sensorData.Data = append(g.Data[row.Id].Data, point)
		g.Data[row.Id] = sensorData
	} else {
		points := make([]GraphPoint, 0)
		points = append(points, point)
		g.Data[row.Id] = SensorData{
			Name: row.Name,
			Data: points,
		}
	}
}

// Keep track of stats for a dataset
type SvgContext struct {
	minTime  int64
	maxTime  int64
	minTemp  int64
	maxTemp  int64
	width    int64
	height   int64
	xAxisPad int64
	yAxisPad int64
}

func (c SvgContext) timeToX(t int64) float64 {
	return float64(c.xAxisPad) + (float64(t-c.minTime)/float64(c.maxTime-c.minTime))*float64(c.width)
}

func (c SvgContext) tempToY(temp float64) float64 {
	fHeight := float64(c.height)
	tempRange := float64(c.maxTemp - c.minTemp)
	return fHeight - (((temp - float64(c.minTemp)) / tempRange) * fHeight)
}

func (c *SvgContext) updateMinMax(row GraphQueryRow) {
	timeInt := int64(row.ReadingDate)
	tempInt := int64(row.Temperature)
	if timeInt > c.maxTime {
		c.maxTime = timeInt
	}
	if timeInt < c.minTime {
		c.minTime = timeInt
	}
	if tempInt > c.maxTemp {
		c.maxTemp = tempInt
	}
	if tempInt < c.minTemp {
		c.minTemp = tempInt
	}
}

func truncateSpan(span string) string {
	switch span {
	case oneHour:
		return "1 minute"
	case oneDay:
		return "10 minutes"
	case oneWeek:
		return "1 hour"
	case oneMonth:
		return "3 hours"
	}

	return "1 minute"
}

func intervalForSpan(span string) string {
	switch span {
	case oneHour:
		return "-1 hour"
	case oneDay:
		return "-1 day"
	case oneWeek:
		return "-7 day"
	case oneMonth:
		return "-30 day"
	}

	return "-1 hour"
}

type VerticalAxisLegend struct {
	Height      float64
	Temperature int64
}

func getVerticalScale(c SvgContext) []VerticalAxisLegend {
	numberLines := c.maxTemp - c.minTemp
	legendLines := make([]VerticalAxisLegend, numberLines)
	for i := c.minTemp; i < c.maxTemp; i++ {
		legendLines = append(legendLines, VerticalAxisLegend{Temperature: i, Height: c.tempToY(float64(i))})
	}

	return legendLines
}

const (
	anHourAndOneMinute = 60 * 61
	twentyFourHours    = 24 * 60 * 60
	aWeek              = 7 * twentyFourHours
)

func getTimescale(c SvgContext) []float64 {
	timeDiff := c.maxTime - c.minTime
	var timeStepSize int64
	if timeDiff > 0 && timeDiff < anHourAndOneMinute {
		timeStepSize = 5 * 60 // 5 minutes
	} else if timeDiff >= anHourAndOneMinute && timeDiff < twentyFourHours {
		timeStepSize = 60 * 60 // An hour
	} else if timeDiff >= twentyFourHours && timeDiff < aWeek {
		timeStepSize = 4 * 60 * 60 // Four hours
	} else {
		timeStepSize = 24 * 60 * 60
	}

	times := make([]float64, 0)

	for i := c.minTime; i < c.maxTime; i += timeStepSize {
		times = append(times, c.timeToX(i))
	}

	return times
}

type GraphMetadata struct {
	XPadding int64
	YPadding int64
	MinTime  int64
	MaxTime  int64
	MinTemp  int64
	MaxTemp  int64
}

type GraphLine struct {
	Id        int64
	SvgPath   string
	LineColor string
}

func drawSvgLine(c SvgContext, id int64, s SensorData) GraphLine {
	color := getSensorColorForId(id)
	op := "M"
	firstInstructionDone := false

	sort.Slice(s.Data, func(i, j int) bool {
		return s.Data[i].Time > s.Data[j].Time
	})

	instruction := ""
	for _, v := range s.Data {
		xPix := c.timeToX(v.Time)
		yPix := c.tempToY(v.Temperature)
		instruction = fmt.Sprintf("%s%s%f,%f ", instruction, op, xPix, yPix)
		if !firstInstructionDone {
			firstInstructionDone = true
			op = "L"
		}
	}

	return GraphLine{Id: id, SvgPath: instruction, LineColor: color}
}

// Hi I'm lib/pq and I eat glue. Why would anyone need to pass a param to `interval`?
func unfuckSqlParameters(query string, param1 string, param2 string) string {
	s1 := strings.Replace(query, "$1", param1, -1)
	s2 := strings.Replace(s1, "$2", param2, -1)
	return s2
}

func prettyDate(t int64) string {
	return time.Unix(t, 0).Format("01/02 03:04 PM")
}

func GetGraphForTime(c *gin.Context) {
	span := c.DefaultQuery("time", oneHour)
	dbContext := GetDbContext(c)
	db := dbContext.Db
	queryStore := dbContext.Queries

	truncationSpan := truncateSpan(span)
	interval := intervalForSpan(span)
	graphQueryFucked := queryStore["graph.graph_temperatures"]
	graphQueryUnFucked := unfuckSqlParameters(graphQueryFucked, truncationSpan, interval)

	rows, err := db.Query(graphQueryUnFucked)
	if err != nil {
		c.HTML(http.StatusInternalServerError, "error.page.html", gin.H{
			"ErrorMessage": err,
		})
		return
	}
	defer rows.Close()

	ctx := SvgContext{
		minTime:  math.MaxInt64,
		maxTime:  0,
		minTemp:  math.MaxInt64,
		maxTemp:  0,
		width:    800,
		height:   800,
		xAxisPad: 40,
		yAxisPad: 40,
	}
	results := GraphDataSet{Data: make(map[int64]SensorData, 0)}
	rowCount := 0
	for rows.Next() {
		var graphQueryRow GraphQueryRow
		if err := rows.Scan(&graphQueryRow.Id, &graphQueryRow.Name, &graphQueryRow.ReadingDate, &graphQueryRow.Temperature); err != nil {
			c.HTML(http.StatusInternalServerError, "error.page.html", gin.H{
				"ErrorMessage": err,
			})
			return
		}

		rowCount += 1

		results.addRow(graphQueryRow)
		ctx.updateMinMax(graphQueryRow)
	}

	ctx.minTemp -= 1
	ctx.maxTemp += 1

	sensorLegends := make([]SensorLegend, 0)
	graphLines := make([]GraphLine, 0)
	for id, data := range results.Data {
		graphLine := drawSvgLine(ctx, id, data)
		graphLines = append(graphLines, graphLine)
		sensorLegends = append(sensorLegends, SensorLegend{
			Id:    id,
			Name:  data.Name,
			Color: getSensorColorForId(id),
		})
	}

	tempAxisLines := getVerticalScale(ctx)
	timeScale := getTimescale(ctx)
	meta := GraphMetadata{
		XPadding: ctx.xAxisPad,
		YPadding: ctx.yAxisPad,
		MinTime:  ctx.minTime,
		MaxTime:  ctx.maxTime,
		MinTemp:  ctx.minTemp,
		MaxTemp:  ctx.maxTemp,
	}

	jString, err := json.Marshal(results)
	if err != nil {
		c.HTML(http.StatusInternalServerError, "error.page.html", gin.H{
			"ErrorMessage": err,
		})
		return
	}

	jMeta, err := json.Marshal(meta)
	if err != nil {
		c.HTML(http.StatusInternalServerError, "error.page.html", gin.H{
			"ErrorMessage": err,
		})
		return
	}

	rawJson := template.HTML(string(jString[:]))
	rawMeta := template.HTML(string(jMeta[:]))

	c.HTML(http.StatusOK, "graph.page.html", gin.H{
		"SensorLegends":  sensorLegends,
		"GraphLines":     graphLines,
		"TempAxisLegend": tempAxisLines,
		"TimeAxisLegend": timeScale,
		"JsonLookup":     rawJson,
		"JsonMeta":       rawMeta,
		"MinTime":        prettyDate(ctx.minTime),
		"MaxTime":        prettyDate(ctx.maxTime),
	})
}

#!/usr/bin/bash

if [ -d "./rel" ]; then
  rm -rf ./rel
fi

go build

mkdir rel
./getBulma.sh
cp -r ./public ./rel
cp -r ./sql ./rel
cp -r ./templates ./rel
cp ./tempView ./rel
cp ./tempView.service ./rel
tar -czvf release.tar.gz rel
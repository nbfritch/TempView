package main

import (
	"fmt"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

type SensorTemperature struct {
	Id          int64
	Name        string
	Temperature float64
	MinutesAgo  string
}

const latestTempsQuery = "temperatures.latest_temperatures"

func approximateMinuteDifference(t int64) string {
	minutes := (time.Now().UTC().Unix() - t) / 60
	return fmt.Sprintf("%d minutes ago", minutes)
}

func GetLatestTemps(c *gin.Context) {
	db := GetDbContext(c)

	fmt.Println(db.Queries[latestTempsQuery])
	rows, err := db.Db.Query(db.Queries[latestTempsQuery])

	if err != nil {
		fmt.Println("Err")
		c.HTML(http.StatusInternalServerError, "error.page.html", gin.H{
			"ErrorMessage": err,
		})
		return
	}
	defer rows.Close()

	var latestTemps []SensorTemperature
	for rows.Next() {
		var latestTemp SensorTemperature
		var timestamp float64
		if err := rows.Scan(&latestTemp.Id, &latestTemp.Name, &latestTemp.Temperature, &timestamp); err != nil {
			fmt.Println("Err1")
			c.HTML(http.StatusInternalServerError, "error.page.html", gin.H{
				"ErrorMessage": err,
			})

			return
		}
		latestTemp.MinutesAgo = approximateMinuteDifference(int64(timestamp))

		latestTemps = append(latestTemps, latestTemp)
	}

	fmt.Printf("Len: %d", len(latestTemps))

	c.HTML(http.StatusOK, "index.page.html", gin.H{
		"temps": latestTemps,
	})
}

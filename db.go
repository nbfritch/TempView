package main

import (
	"database/sql"
	"errors"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"

	_ "github.com/lib/pq"
)

type DbContext struct {
	Db      *sql.DB
	Queries map[string]string
}

const (
	DbHostVar     = "DB_HOST"
	DbPortVar     = "DB_PORT"
	DbUserVar     = "DB_USER"
	DbPassVar     = "DB_PASS"
	DbDatabaseVar = "DB_DATABASE"
	DbUseSslVar   = "DB_USE_SSL"
)

func ReadDatabaseVariables() (string, error) {
	host := os.Getenv(DbHostVar)
	port := os.Getenv(DbPortVar)
	user := os.Getenv(DbUserVar)
	password := os.Getenv(DbPassVar)
	database := os.Getenv(DbDatabaseVar)
	useSsl := os.Getenv(DbUseSslVar)

	var sslMode = "disable"
	if useSsl == "TRUE" {
		sslMode = "enable"
	}

	if len(host) < 1 {
		return "", fmt.Errorf("ERR: Got host: '%s'", host)
	}

	if len(user) < 1 {
		return "", fmt.Errorf("ERR: Got user: '%s'", user)
	}

	if len(port) < 2 {
		return "", fmt.Errorf("ERR: Invalid port: %s", port)
	}

	if len(password) < 1 {
		return "", fmt.Errorf("ERR: Invalid password of len %d", len(password))
	}

	if len(database) < 1 {
		return "", fmt.Errorf("ERR: Invalid database '%d'", len(database))
	}

	dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s sslmode=%s", host, user, password, database, port, sslMode)

	return dsn, nil
}

func SetupDb() *sql.DB {
	connStr, err := ReadDatabaseVariables()
	if err != nil {
		log.Fatal(err)
	}

	db, err := sql.Open("postgres", connStr)
	if err != nil {
		log.Fatal(err)
	}

	if err := db.Ping(); err != nil {
		log.Fatal(err)
	}

	return db
}

func nameQuery(file string) string {
	s1 := strings.ReplaceAll(file, "sql/", "")
	s2 := strings.ReplaceAll(s1, ".sql", "")
	return strings.ReplaceAll(s2, "/", ".")
}

func LoadQueriesFromDisk() map[string]string {
	matches, err := filepath.Glob("sql/**/*.sql")
	if err != nil {
		log.Fatal(err)
	} else if len(matches) == 0 {
		log.Fatal(errors.New("failed to load queries"))
	}

	queries := make(map[string]string)

	for _, m := range matches {
		queryName := nameQuery(m)
		_, hasValue := queries[queryName]
		if hasValue {
			log.Fatal(fmt.Errorf("attempted to load two queries with the same name: %s", m))
		}
		content, err := os.ReadFile(m)
		if err != nil {
			log.Fatal(err)
		}
		queries[queryName] = string(content)
		fmt.Printf("Loaded query %s as %s\n", m, queryName)
	}

	return queries
}

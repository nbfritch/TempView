package main

import (
	"fmt"
	"os"

	"github.com/gin-gonic/gin"
)

func main() {
	db := SetupDb()

	queryStore := LoadQueriesFromDisk()

	dbStore := DbContext{Db: db, Queries: queryStore}

	r := gin.Default()

	r.Use(RegisterDbContext(dbStore))

	ConfigureRoutes(*r)

	r.LoadHTMLGlob("templates/**/*")
	webPort := os.Getenv("WEB_PORT")

	if len(webPort) < 2 {
		webPort = "5000"
		fmt.Printf("No port provided, using %s", webPort)
	}

	r.Run(fmt.Sprintf("0.0.0.0:%s", webPort))
}
